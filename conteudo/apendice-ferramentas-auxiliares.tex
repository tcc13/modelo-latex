%% ------------------------------------------------------------------------- %%
\chapter{Ferramentas de Desenvolvimento Auxiliares}
\label{ap:ferramentas_utilizadas} 

O processo de desenvolvimento de código envolve uma série de etapas. 
\cite{vithani2014modeling} propuseram um ciclo de 
desenvolvimento para aplicações móveis, o MADLC (\emph{Mobile Application 
Development Lifecycle}), que possui as seguintes
etapas: Identificação, \emph{Design}, Desenvolvimento, Prototipação, Teste, 
Entrega e Manutenção.
Para isso é inevitável o uso de ferramentas auxiliares. Por exemplo, durante esse 
estudo foram utilizadas ferramentas de design e prototipação,
de versionamento, de edição de código, de testes e de integração e entrega 
continuas.

A seguir serão apresentadas as ferramentas utilizadas, que foram:

\section{Marvel}
\label{sec:marvel}

Como citado no capítulo \ref{cap:prototipo}, a ferramenta de design utilizada
foi o Marvel\footnote{Link do Marvel: \url{https://marvelapp.com/}. 
Acesso em 15 Ago 2021}.   
Ele é uma ferramenta que permite um rápido processo de prototipação, teste e entrega
do resultado final para o time. Ele foi escolhido principalmente por sua 
funcionalidade de construir um protótipo navegável a partir de desenhos no papel. 
Isso foi importante principalmente nos estágios iniciais da idealização do aplicativo.
Mas também mostrou-se uma ferramenta intuitiva e poderosa nas etapas seguintes.

\section{Gitlab}
\label{sec:gitlab}

O Gitlab\footnote{Link do Gitlab: \url{https://about.gitlab.com}. 
Acesso em 15 Ago 2021}
é a ferramenta de DevOps mais utilizada do mercado, com uma estimativa 
de mais de 30 milhões de usuários registrados \citep{gitlabMostUsed}.
DevOps, segundo \cite{jabbari16:devops}, é uma metodologia que faz 
a ponte entre desenvolvimento e operações, com ênfase na comunicação e colaboração,
integração continua, garantia de qualidade, entrega continua com implantação automática 
utilizando uma série de práticas de desenvolvimento. O grande motivo dessa 
ferramenta ser a mais utilizada deve-se ao fato dela fazer essa ponte de forma 
completa, através do versionador de código integrado ao 
serviço de integração e entrega contínua e da quantidade de possíveis
integrações com plataformas externas.

Para esse projeto, o Gitlab foi útil principalmente pela fácil
integração com a plataforma Bitrise, que será apresentada na subseção 
\ref{sec:bitrise}.
Essa integração foi feita utilizando uma estratégia de acionamento de gatilhos 
do Bitrise com base em atualizações das duas principais ramificações de código 
utilizadas, \emph{master} e \emph{develop}.
O desenvolvimento foi feito sempre com base no código da ramificação 
\emph{develop}, que acionava um gatilho específico de desenvolvimento. 
A ramificação \emph{master}, por sua vez, era atualizada sempre que houvesse uma 
versão estável da aplicação e gerasse valor para o usuário, acionando
consequentemente outro gatilho configurado especificamente para produção.


\section{Visual Studio Code}
\label{sec:vscode}

A ferramenta utilizada para edição de código foi o Visual Studio Code 
(VS Code)\footnote{Link do Visual Studio Code: 
\url{https://code.visualstudio.com/docs}. 
Acesso em 15 Ago 2021},
que é um leve, mas poderoso editor de código-fonte desenvolvido pela Microsoft. 
Uma de suas principais vantagens, além de ser leve, como citado anteriormente, é 
que ele possui diversas integrações poderosas (chamadas extensões). Essas
extensões facilitam o desenvolvimento, pois agregam diversas funcionalidades
durante a escrita do código e aumentam a produtividade. 
Por exemplo, foram utilizadas extensões de formatação de código, de 
integrações com ferramentas de teste, de integração com versionadores de 
código, de IntelliSense (conhecido como assistente de código).

Vale a pena comentar que foi analisada a possibilidade de utilizar o 
Android Studio\footnote{Link do Android Studio: 
\url{https://developer.android.com/studio}. Acesso em 15 Ago 2021}, 
que é a ferramenta de edição de 
código oficial para desenvolvimento
Android \citep{ASOfficial}. No entanto, ela exige bastante dos recursos do computador para
apresentar um bom desempenho e apesar de também possuir algumas integrações
como a do versionador e também assistente de código, não são bem construídas
como no VS Code. A grande vantagem do Android Studio é a integração com o 
emulador Android, que permite testar o aplicativo diretamente do computador
sem precisar de um dispositivo. Contudo, a funcionalidade de realidade aumentada
depende de câmera, por isso não faz sentido usar um emulador e sim o próprio 
dispositivo. Sendo assim, a utilização dessa ferramenta se tornou desnecessária.

\section{Ferramentas de teste}
\label{sec:ferramentas_de_teste}

Para os testes foram utilizadas duas ferramentas em cada linguagem, elas são
diferentes, porém, com propósitos parecidos.
Para Flutter foi utilizado o conjunto flutter\_test\footnote{Link do flutter\_test: 
\url{https://api.flutter.dev/flutter/flutter_test/flutter_test-library.html}. 
Acesso em 15 Ago 2021} 
e o mockito\footnote{Link do mockito:
\url{https://pub.dev/packages/mockito}. 
Acesso em 15 Ago 2021}, 
enquanto que para React Native
foi utilizado o Jest\footnote{Link do Jest: 
\url{https://jestjs.io/docs/tutorial-react-native}. 
Acesso em 15 Ago 2021} e a 
Testing Library\footnote{Link do Testing Library:
\url{https://testing-library.com/docs/react-native-testing-library/intro/}. 
Acesso em 15 Ago 2021}. 
Tanto o flutter\_test quanto a Testing Library foram utilizados para emular 
componentes visuais e testar as possíveis interações. Já o mockito e o Jest foram 
utilizados para simular requisições e bibliotecas no ambiente de teste.

Essas ferramentas foram utilizadas para dois tipos de testes, testes unitários e 
de integração. Os testes unitários foram implementados para validar 
principalmente as regras de negócio da aplicação e os testes de integração foram
utilizados para validar de forma automatizada as funcionalidades existentes. 


\section{Bitrise}
\label{sec:bitrise}

O Bitrise\footnote{Link do Bitrise: \url{https://www.bitrise.io/}. Acesso em 15 Ago 2021}
é uma ferramenta de integração e entrega continua para aplicações móveis.
Ela oferece mais de 300 passos e integrações para conectar processos e serviços que 
geralmente são necessários durante o fluxo de desenvolvimento móvel. O ponto forte
dessa plataforma é a fácil customização desses passos e integrações para
a criação de fluxos específicos para cada necessidade.

Nesse caso, foram criados dois fluxos principais, um para teste e outro para implantação. 
O fluxo de teste foi configurado para ser acionado a partir de uma atualização 
da ramificação \emph{develop} e executar a rotina de testes implementadas. 
Já o fluxo de implantação foi customizado para ser acionado apenas com a 
atualização da \emph{master}. Esse fluxo além de executar os testes, também executa os 
passos necessários para a geração do \emph{Android Application Package} (APK), 
arquivo que depois pode ser utilizado para disponibilização na loja de aplicativos,
Google Play. Como a geração da APK é último passo em que as duas linguagens se 
diferem, o fluxo de implantação foi configurado até esse ponto apenas. 