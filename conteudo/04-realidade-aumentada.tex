%!TeX root=../tese.tex
%("dica" para o editor de texto: este arquivo é parte de um documento maior)
% para saber mais: https://tex.stackexchange.com/q/78101/183146

%% ------------------------------------------------------------------------- %%
\chapter{Realidade Aumentada}
\label{cap:desenvolvendo_realidade_aumentada}

Como apresentada na \autoref{sec:prototipo_realidade_aumentada}, a realidade
aumentada é uma tecnologia que combina elementos reais com os virtuais. No caso dos dispositivos móveis, a partir da câmera e de outros 
sensores presentes, busca entender o ambiente ao seu redor para que a 
combinação seja o mais fiel possível de uma experiência esperada fisicamente,
ou seja, caso o elemento virtual realmente estivesse ali.

Para implementar a realidade aumentada no protótipo em questão foi utilizada
a plataforma do Google, específica para essa finalidade, chamada ARCore. 
Como diz em sua própria documentação \citep{arcoreDefinition}, essa plataforma 
utiliza diversas APIs para permitir que o telefone perceba o 
ambiente, entenda o mundo e interaja com as informações. 
O ARCore utiliza três funcionalidades para combinar elementos 
virtuais com os elementos reais observados através da câmera:

\begin{itemize}
    \item \textbf{Detecção de movimento}: Permite que o telefone mapeie sua posição
          relativa no mundo;
    \item \textbf{Entendimento do ambiente}: Permite que o telefone identifique tamanho
          e localização de superfícies (verticais e horizontais);
    \item \textbf{Estimativa de luminosidade}: Permite que o telefone estime as 
          condições de luminosidade do ambiente;
\end{itemize}

O ARCore é uma plataforma que pode ser utilizada de várias formas. Pode
ser utilizada diretamente em aplicações Android, IOS ou até mesmo em
\emph{game engines} como a Unity e a Unreal. Nesse projeto as implementações
foram testadas apenas em dispositivos Android, essencialmente por 
não ter acesso a dispositivos IOS durante o desenvolvimento, então fica
para um trabalho futuro testar em IOS e verificar se esse comparativo
também é válido.

A seguir vamos olhar mais a fundo sobre os elementos que tornam a funcionalidade
possível e quais são os desafios da realidade aumentada, principalmente
tratando de dispositivos móveis.

%% ------------------------------------------------------------------------- %%
\section{SLAM}
\label{sec:slam} 

Um dos pontos cruciais dessa tecnologia é a detecção de movimento. Para isso 
o ARCore utiliza um processo chamado SLAM (\emph{Simultaneous Localization and
Mapping}), que permite entender onde o telefone está em relação ao mundo ao seu redor
\citep{arcoreFundamentals}.
Como retratam \citet{SLAM} em seu artigo, o SLAM começou a ser endereçado como 
uma solução para o mapeamento
e localização na robótica em 1986, na conferência de Robôs e Automação da IEEE.
Naquela época, métodos probabilísticos estavam começando a ser inseridos
na comunidade sobre robótica. Uma série de métodos teóricos de estimativa eram 
aplicados pelos pesquisadores a fim de solucionar esse problema. Fora reconhecido
nessa conferência que um mapeamento probabilístico consistente era um problema
fundamental na robótica com grandes limitações conceituais e computacionais ainda.
Somente em 1995 o SLAM foi formulado como um processo em que um robô 
consegue mapear o ambiente enquanto utiliza esse mapa para deduzir 
sua localização sem necessidade de um conhecimento prévio. Esse robô utiliza 
sensores para identificar as marcações definidas no ambiente e a partir da 
sua movimentação e de novas observações consegue inferir sua localização.

As múltiplas observações de uma marcação durante a movimentação 
permitem atualizar a localização estimada do robô e da marcação, bem como
permitem propagar essa atualização para as outras marcações, mesmo que não
sejam observadas a partir da nova localização. Isso ocorre por as 
marcações serem fortemente correlacionadas (suas localizações relativas são bem
conhecidas) a partir das observações anteriores. A medida que novas marcações são
descobertas, elas são imediatamente ligadas ou correlacionadas ao resto do mapa,
dessa forma todas juntas acabam formando uma rede ligada por suas
localizações relativas. Com isso, podemos concluir que a precisão da 
localização do robô relativa ao mapa é limitada apenas pela qualidade do mapa
e pelas medições do sensor. Na figura \ref{fig:exemplo_slam} podemos ver
a trajetória do robô ao longo do tempo, bem como as marcações e suas localizações
estimadas, calculadas conforme as seguintes variáveis:

\begin{itemize}
    \item $x_k$: Representa o vetor de localização e orientação do 
          robô no instante $k$;
    \item $u_k$: Vetor de deslocamento para o estado $x_k$ em relação
          ao instante anterior $k-1$;
    \item $m_i$: Vetor da localização da i-ésima marcação;
    \item $z_{ik}$: Observação da i-ésima marcação no instante de tempo \emph{k};
\end{itemize}

\begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{conceito-slam}
    \caption{
        Exemplo da trajetória do robô ao longo dos instantes $k-1$ a $k+2$
        observando as marcações $m$ \citep{SLAM}.
        \label{fig:exemplo_slam}
    }
\end{figure}

A partir dessas informações é possível calcular a distribuição probabilística
para cada instante \emph{k}. Nesse caso usamos uma distribuição posteriori 
conjunta para estimar a posição do robô ($x_k$), bem como a posição das 
marcações ($m$), dadas observações das marcações ($Z_{0:k}$), os 
descolamentos realizados até o momento ($U_{0:k}$), e a posição inicial 
do robô ($x_0$), como a probabilidade a seguir

\begin{equation}
    P(x_k, m | Z_{0:k}, U_{0:k}, x_0)
\end{equation}

Na prática, esse cálculo é feito de forma recursiva a partir da probabilidade
calculada no instante $k-1$ utilizando o Teorema de Bayes. Para isso são necessários
dois modelos, um para movimentações e outro para as observações, 
representados da seguinte forma respectivamente:

\begin{equation}
    P(x_k | x_{k-1}, u_k)
\end{equation}

\begin{equation}
    P(z_k | x_k, m)
\end{equation}

Com isso o algoritmo do SLAM pode ser representado como a seguinte 
recursão de duas etapas

\begin{equation}
    P(x_k, m | Z_{0:k-1}, U_{0:k}, x_0) = 
    \int
        P(x_k|x_{k-1}, u_k) \times
        P(x_{k-1, m | Z_{0:k-1}, U_{0:k-1}, x_0}) 
    \,dx_{k-1}
\end{equation}

\begin{equation}
    P(x_k, m | Z_{0:k}, U_{0:k}, x_0) = 
    \frac
        {P(z_k|x_k,m) P(x_k, m | Z_{0:k-1}, U_{0:k}, x_0)}
        {P(z_k| Z_{0:k-1}, U_{0:k})}
\end{equation}


Tendo em mente a estrutura do problema, as soluções
buscam encontrar uma representação apropriada para 
os modelos de movimentação e observação para otimizar
a recursão acima, sendo os algoritmos EKF-SLAM e FastSLAM dois dos 
mais importantes.

%% ------------------------------------------------------------------------- %%
\subsection{EKF-SLAM}
\label{sec:ekf} 

O EKF-SLAM é um algoritmo que aplica o \emph{Extended Kalman Filter} (EKF) para
estimar um valor para os modelos de movimentação e observação do SLAM. Para 
entender melhor o algoritmo, vale 
explicar de forma breve os conceitos por trás dele.
O \emph{Kalman Filter} (KF) é um sistema linear, 
discreto e com variação de tempo finita que calcula uma estimativa para um estado
de forma a minimizar o erro quadrático médio \citep{KalmanFilter}. 
Em uma abordagem mais prática, o KF consiste em três equações, cada uma envolvendo 
manipulação de matrizes \citep{simon2001kalman}, que são:

\begin{equation}
    K_k = AP_kC^T(CP_kC^T + S_z)^{-1}
\end{equation}

\begin{equation}
    \hat{x}_{k+1} = (A\hat{x}_k + Bu_k) + K_k(Y_{k+1} - C\hat{x}_k)
\end{equation}

\begin{equation}
    P_{k+1} = AP_kA^T + S_w - AP_kC^TS_z^{-1}CP_kA^T
\end{equation}

Dessa forma, podemos observar que a estimativa de um estado no instante $k+1$ é 
calculada recursivamente de acordo com alguns valores no instante $k$.
As variáveis para esse cálculo são as seguintes:

\begin{itemize}
    \item $K_k$: Ganho de Kalman, que minimiza a covariância do erro;
    \item $A,B,C$: São matrizes que fazem parte das equações de entrada
          ($x_{k+1} = Ax_k + Bu_k + w_k$) e saída
          ($y_k = Cx_k + z_k$);
    \item $w_k, z_k$: São ruídos de processo e de medição, respectivamente;
    \item $S_w, S_z$: São as matrizes de covariância dos ruídos de processo e
          medição, respectivamente;
    \item $P_k$: Estimativa da matriz de covariância do erro no instante $k$.
          Note que ela também é calculada de forma recursiva, de acordo com 
          o valor da estimativa anterior;
\end{itemize}

Compreendendo o KF, fica mais fácil entender o EKF, que nada mais 
é que a utilização do KF para sistemas com funções não-lineares. 
Esse é o caso do SLAM, na qual os modelos de movimentação e observação
podem descritos, respectivamente, como

\begin{equation}
    P(x_k | x_{k-1}, u_k) \Longleftrightarrow x_k = f(x_{k-1}, u_k) + w_k
\end{equation}

\begin{equation}
    P(z_k | x_k, m) \Longleftrightarrow z_k = h(x_k, m) + v_k
\end{equation}

Nessa nova representação, as funções f e h são não-lineares
e $w_k$ e $v_k$ são os ruídos com distribuição normal. Como KF assume 
uma distribuição Gaussiana, para que suas propriedades sejam mantidas é 
necessário realizar um processo de linearização dessas funções em relação 
à estimativa atual \citep{KalmanFilter}.

Pensando em complexidade
computacional, essa abordagem pode não ser tão eficiente, pois a cada
nova observação feita é necessário recalcular a matriz de covariância P,
significando que a computação cresce de forma quadrática em relação ao 
número de marcações \citep{SLAM},
dificultando o processamento em tempo real para ambientes maiores.
% Apesar dessa solução inicial ser $O(n^2)$, estudos mais recentes \citep{slamOn}
% mostram que é possível aplicar uma estratégia de divisão e conquista para
% conseguir complexidade uma complexidade $O(n)$, que torna esse método mais
% atrativo para uso em tempo real.

%% ------------------------------------------------------------------------- %%
\subsection{FastSLAM}
\label{sec:fastslam}

O FastSLAM é um algoritmo que traz uma reformulação para o problema de
SLAM. Ele parte da constatação de que conhecer o caminho do robô torna as 
medições das marcações independentes, dessa forma, determinar 
as marcações pode ser desacoplado em diversos problemas de estimação diferentes,
uma para cada marcação \citep{fastslam}.
Dada essa independência condicional, podemos reformular a probabilidade 
original substituindo a posição do 
robô no instante $k$ ($x_k$) por $X_{0:k}$, que 
representa a trajetória do robô até o instante $k$. Isso implica que a 
posteriori pode ser calculada, portanto, da seguinte forma:

\begin{equation}
    P(X_{0:k}, m | Z_{0:k}, U_{0:k}, x_0) = 
        P(X_{0:k} | Z_{0:k}, U_{0:k}, x_0) 
        \prod_{k}P(m_k | X_{0:k}, Z_{0:k})
\end{equation}

A partir dessa reformulação, o FastSLAM aplica o conceito
de Rao-Blackwellization 
(R-B), que diz que um estado conjunto pode ser particionado de acordo com a
regra do produto $P(x_1,x_2) = P(x_2|x_1)P(x_1)$ e se $P(x_2|x_1)$ puder ser 
representado de forma analítica, então apenas $P(x_1)$ precisa ser 
amostrado $x_1^{(i)} \sim P(x_1)$ \citep{SLAM}. Considerando esse contexto,
podemos aplicar \emph{Particle Filtering} 
(PF) para calcular a posteriori da trajetoria $P(X_{0:k} | Z_{0:k}, U_{0:k}, x_0)$
e aplicar KF para estimar a posição das marcações $P(m_k | X_{0:k}, Z_{0:k})$. Essa 
abordagem é bem interessante, pois diminui a quantidade de amostras necessárias
para se obter uma boa aproximação, o grande problema do PF.

Para entender melhor o algoritmo, vamos olhar o que é o PF e como ele e o KF 
são aplicados. Começando pelo PF, a ideia dele é representar uma posteriori
por um conjunto de amostras retirados dessa posteriori. Essa
representação é aproximada, mas é não-parametrizada, então fica livre para 
ser aplicada a mais distribuições além da Gaussiana \citep{probabilisticRobotics}, 
que o KF assume, por exemplo. Na prática, ele pode ser descrito pelas seguintes
etapas, executadas de forma recursiva, que são:

\begin{itemize}
    \item \textbf{Amostragem}: Nessa etapa, para cada uma das $M$ amostras, 
          é estimado um novo estado com base no anterior e no vetor de
          deslocamento. Essa estimativa é feita a partir da distribuição
          de transição de estado;
    \item \textbf{Fator de importância}: É calculado para cada amostra
          a razão entre a distribuição desejada e a distribuição proposta.
          Essa razão ajuda a ponderar a diferença entre as distribuições,
          como um fator de correção entre elas;
    \item \textbf{Reamostragem}: Esse processo basicamente faz $M$ retiradas com 
          reposição do conjunto atual, na qual a probabilidade de uma 
          amostra ser retirada é proporcional ao peso dela. 
          Cada amostra retirada é adicionada em um novo conjunto de 
          amostras;
\end{itemize} 

A aplicação do PF ao problema do SLAM consiste na utilização do modelo 
probabilístico da movimentação para gerar um conjunto de amostras que 
representam as hipóteses da trajetória e assim realizar as atualizações
conforme o processo descrito acima.

Como o FastSLAM utiliza essa abordagem de amostragem das trajetórias e
como a estimativa das marcações dependem delas, o KF está
associado individualmente a uma amostra. Portanto, para $M$ amostras e 
$K$ marcações, isso vai resultar num total de $KM$ \emph{Kalman Filters},
cada um de dimensão 2, devido às duas coordenadas das marcações \citep{fastslam}.
A grande vantagem desse algoritmo em relação ao EKF-SLAM é justamente o ganho
de desempenho, que passa a ser $O(KM)$ em vez de ser $O(K^2)$, por utilizar
pequenas matrizes de covariância P em vez de uma única matriz de dimensão
$K \times K$.

% Apresentar o VISLAM - ARCore fundamentals + Localization Limitations (LL) 
% - Adiciona o visual e outros sensores para melhorar o SLAM
% - Features sintéticas e naturais

%% ------------------------------------------------------------------------- %%
\section{VISLAM}
\label{sec:vislam}

Vimos que uma parte importante do SLAM é a identificação de uma marcação para
medir sua posição em relação ao robô. Quando
falamos de realidade aumentada, não estamos falando de robôs avaliando 
marcações, estamos falando do ser humano interagindo com um 
dispositivo para gerar a combinação do real com o virtual. Para isso o 
\emph{visual-inertial} SLAM (VISLAM) utiliza informações 
visuais combinadas com medições inerciais de giroscópios ou acelerômetros, 
por exemplo, para estimar a posição e orientação da câmera em relação ao mundo.
Essa é a abordagem utilizada pelo ARCore \citep{arcoreFundamentals}, que 
se aproveita do fato 
dos novos celulares hoje serem equipados com câmeras de qualidade
cada vez melhores
e também dispositivos de medição inercial para fazer com que essa 
funcionalidade seja cada vez mais viável.

De acordo com o estudo apresentado por \citet{vislam}, existem dois grandes
desafios computacionais para o VISLAM: O desafio do \emph{frontend} e do 
solucionador. O \emph{frontend} inclui as tarefas de extração e correlação de 
pontos importantes observados. Essa parte pode ser paralelizável e realizada
de forma eficiente, em geral. Já a tarefa do solucionador é 
combinar as informações visuais e medições inerciais para otimizar as 
funções objetivo do SLAM apresentadas anteriormente (função de movimentação
e observação).
O VISLAM não necessita de muitos pontos importantes para atingir uma 
precisão razoável, uma vez que as medições inerciais fornecem restrições 
adicionais, porém a depender da solução utilizada e da 
implementação essa tarefa pode ser de grande custo computacional, 
como foi discutido anteriormente.

% feature points -> São difíceis de detectar em lugares com pouca textura
As tarefas do solucionador, em geral, são o gargalo para esse 
tipo de abordagem, uma vez que elas tem complexidade computacional maior 
em relação as de \emph{frontend}. Contudo, problemas na identificação dos pontos
importantes podem fazer com que essa segunda tarefa seja o maior dificultador 
desse algoritmo.

% [TODO]: Escrever sobre a complexidade na detecção de pontos importantes, 
% principalmente em lugares com pouca textura (Visão computacional).

Exitem vários métodos para a identificação de pontos importantes, vamos
abordar dois métodos importantes nessa área, o \emph{Harris Corner
Detector} e o \emph{Scale Invariant Feature Transform}(SIFT).

%% ------------------------------------------------------------------------- %%
\subsection{Harris Corner Detector}
\label{sec:harris_corner_detector}

Segundo a análise feita por \citet{HarrisCornerDetector}, o método 
\emph{Harris Corner Detector}, apesar do surgimento de novas técnicas,
continua sendo uma referência. Ainda em sua pesquisa, 
eles definem esse método como uma técnica para localizar 
pontos importantes em uma imagem a partir de uma função de autocorrelação,
usada para medir a intensidade das diferenças encontradas em uma 
janela dessa imagem aplicando diversos deslocamentos.

De forma prática, esse método utiliza soma do quadrado das diferenças e busca
encontrar cantos através de uma janela de pontos $x$ que maximizam essa 
função para pequenos deslocamentos h:

\begin{equation}
    E(h) = \sum_{x} w(x) (I(x + h) - I(x))^2
\end{equation}

Na função acima, $I(x)$ representa uma função de intensidade da imagem e $w(x)$ 
descreve a região de aceitação, em geral, definido por uma função Gaussiana.
Aplicando a expansão da série de Taylor, podemos aproximar 
$I(x + h) \simeq I(x) + \nabla I(x)^Th$. Substituindo essa aproximação na 
função original e simplificando ela, chegamos na seguinte aproximação:

\begin{equation}
    E(h)
    \simeq 
    \sum_{x} w(x) (h^T \nabla I(x) \nabla I(x)^T h) 
    = 
    h^T 
    \bigg(
        \sum_{x} w(x)
        \begin{bmatrix}
            I_x^2 & I_xI_y\\
            I_xI_y & I_y^2\\
        \end{bmatrix}
    \bigg)
    h
\end{equation}

Analisando a forma final dessa aproximação podemos concluir que o valor máximo
pode ser encontrado analisando a seguinte matriz $M$, chamada matriz de
autocorrelação:

\begin{equation}
    M =
    \sum_{x} w(x)
    \begin{bmatrix}
        I_x^2 & I_xI_y\\
        I_xI_y & I_y^2\\
    \end{bmatrix}
\end{equation}

A partir dessa matriz M, \citet{Harris88acombined} propuseram uma
medida de resposta $R$ para a detecção de cantos, que dispensa a
utilização de seus autovalores $\lambda_1$ e $\lambda_2$ de forma 
explicita. 
Portanto, a partir do
$Det(M) = \lambda_1 \lambda_2 = I_x^2I_y^2 - (I_xI_y)^2$,
$Tr(M) = \lambda_1 + \lambda_2 = I_x^2 + I_y^2$ e de uma constante
$k$ (geralmente entre 0,04 e 0,06), o valor de $R$ pode ser calculado: 

\begin{equation}
    R = Det(M) - k Tr(M)^2
\end{equation}

O valor de $R$ pode ser traduzido nos seguintes resultados:

\begin{itemize}
    \item \textbf{$R$ é negativo}: Significa que a diferença entre os autovalores é
            grande e o \emph{pixel} provavelmente pertence a uma aresta;
    \item \textbf{$R$ é positivo e alto}: Os dois autovalores são altos e provavelmente
            é um canto;
    \item \textbf{$R$ é positivo e baixo}: Os dois autovalores são baixos e faz parte
            de uma região plana;
\end{itemize}

Esse método é eficaz para o
mapeamento de cantos, arestas e regiões planas e pode ser resolvido para 
otimizar o custo computacional como propõe \citet{optimizedHarris}. 
No entanto, a falta de
cantos e arestas pode dificultar uma real compreensão da imagem.

%% ------------------------------------------------------------------------- %%
\subsection{SIFT}
\label{sec:sift}

O segundo método para identificação de pontos importantes que vamos abordar
é o \emph{Scale Invariant Feature Transform} (SIFT). Esse método foi proposto
por David G. Lowe e tem como objetivo transformar as informações de uma imagem
em coordenadas resistentes a escala relativas a pontos locais 
\citep{lowe2004distinctive}. Essa abordagem busca descrever pontos importantes
de forma única, para ser possível correlacionar um ponto de forma eficiente
quando comparado a pontos previamente calculados de um banco de imagens.
Para chegarmos nessa descrição são necessárias quatro etapas:
Detecção de extremos em espaço de escala, Localização de pontos chaves,
Atribuição de Orientação e Descritor de pontos-chave.

Na primeira etapa de detecção de extremos em espaço de escala, são 
identificados candidatos a pontos-chave através de uma função de Diferença 
de Gaussianas (DOG). A ideia principal dessa etapa é criar diversas
cópias da imagem e aplicar de forma incremental a convolução 
de uma função Gaussiana com a imagem, a partir de um fator de escala 
$\sigma$. Portanto, definimos a função $L$ 
como sendo a imagem suavizada pela função Gaussiana $G$ e $D$ é a 
diferença de duas Gaussianas separadas por uma constante $k$, como 
representado abaixo: 

\begin{equation}
    L(x, y, \sigma) = G(x, y, \sigma) * I(x, y)
    \label{equation:smooth_image}
\end{equation}

\begin{equation}
    G(x, y, \sigma) = \frac{1}{2\pi\sigma^2}e^{-(x^2+y^2) / 2\sigma^2}
\end{equation}

\begin{equation}
    \begin{aligned}
        D(x, y, \sigma) & = (G(x, y, k\sigma) - G(x, y, \sigma)) * I(x, y)\\
                        & = L(x, y, k\sigma) - L(x, y, \sigma)
    \end{aligned}
\end{equation}

Para calcular $D$ de forma eficiente, são utilizadas conjuntos de imagens
chamados de oitavas, na qual, cada oitava deve conter um número $s + 3$
de imagens, sendo $s$ o número de incrementos. Considerando esse número
de incrementos, o valor do incremento entre cada imagem pode ser definido
em função de $s$ na forma $k = 2^{1/s}$. Uma vez finalizado o cálculo de
$D$ em uma oitava, o processo é repetido para a próxima oitava, porém com
um número de amostras da imagem 2 vezes menor.

A partir dos valores calculados de $D$, definimos que um ponto é de 
extremo em espaço de escala  se ele tem valor maior ou menor que todos os 
seus vizinhos, tanto os oito vizinhos de sua imagem atual, quanto os nove 
da escala anterior e os outros nove da posterior.

A segunda etapa de localização de pontos-chave consiste em eliminar 
candidatos apontados na etapa anterior que possuam baixo contraste ou 
que pertençam a arestas, de forma parecida com o que propõe 
o \emph{Harris Corner Detector}. Para eliminar pontos com baixo contraste
é aplicada a expansão da série de Taylor para $D$ e calculado o valor 
aproximado da função para aquele extremo $\hat{x}$ da seguinte forma:

\begin{equation}
    D(\hat{x}) = D + \frac{1}{2} \frac{\partial D^T}{\partial x}\hat{x}
\end{equation}

Se o valor absoluto de $D(\hat{x})$ for menor que um determinado valor, 
então ele será descartado. Já para eliminar 
pontos que pertencem a arestas, assim como no \emph{Harris Corner Detector},
podemos calcular $Det(H)$ e $Tr(H)$ sem conhecimento explicito dos autovalores
de $H$, a matriz Hessiana de $D$. Considerando $r$,
a razão entre os autovalores $\lambda_1$ e $\lambda_2$, para $\lambda_1 > \lambda_2$,
podemos dizer que:

\begin{equation}
    \frac{Tr(H)^2}{Det(H)} = 
    \frac{(\lambda_1 + \lambda_2)^2}{\lambda_1\lambda_2} =
    \frac{(r\lambda_2 + \lambda_2)^2}{r\lambda_2^2} = 
    \frac{(r + 1)^2}{r}
\end{equation}

Logo para decidirmos se vamos eliminar um candidato a ponto-chave, basta
calcular se $Tr(H)^2/Det(H)$ é menor que $(r+1)^2/r$, para um determinado
valor de $r$.

Na etapa de atribuição de orientação, como o próprio nome diz, buscamos
atribuir uma orientação para os pontos chaves filtrados
na segunda etapa. A partir das escalas dos pontos chaves são selecionadas 
as imagens suavizadas $L$ e computados dois valores para cada ponto
dessa imagem, a orientação $\theta$ e a magnitude $m$, através
da diferença entre \emph{pixels} adjacentes:

\begin{equation}
    \theta(x, y) = tan^{-1}((L(x, y+1)-L(x, y-1))/(L(x+1, y)- L(x-1,y)))
\end{equation}

\begin{equation}
    m(x, y) = \sqrt{(L(x+1, y) - L(x-1, y))^2 + (L(x, y + 1) - L(x, y-1))^2}
\end{equation}

Com esses valores é feito um histograma das orientações ao redor de um 
ponto-chave. Esse histograma é separado em 36 intervalos de 10 graus, e o valor
adicionado ao histograma é ponderado pela magnitude naquele ponto e também
por uma janela circular de peso Gaussiano, com $\sigma$ 1,5 vezes maior do
que o daquela escala, assim pontos mais próximos tem maior relevância do
que pontos mais distantes. Após a criação desse histograma, a orientação 
com maior pico é atribuída ao ponto-chave em questão, além disso, 
caso existam picos de magnitude com pelo menos 80\% em relação ao maior, 
são criados outros pontos-chave com a mesma localização e escala, 
porém com essas outras orientações.

Por fim, a etapa descritor de pontos-chave propõe uma descrição mais ampla
do ponto-chave, considerando não só sua localização, escala e orientação,
mas também as características da região ao seu redor, aumentando assim
a robustez do método. Para isso, a abordagem utilizada é parecida com
a etapa anterior, também observa as magnitudes e orientações dos pontos 
próximos ao ponto-chave dentro de uma janela circular definida por uma 
função Gaussiana. Contudo, em vez de criar um histograma para o ponto-chave, 
são criados histogramas de oito intervalos de 45 graus, para cada
região de tamanho 4x4 pertencente a essa janela. As entradas desse 
histograma são também como na etapa anterior, porém considerando apenas
os pontos da região na qual ele representa. Dessa forma, em uma janela
de tamanho 16x16, são criados 4x4 histogramas contendo, cada um, 8
orientações com suas magnitudes associadas. Portanto, o descritor de um
ponto-chave nada mais é que um vetor, que para cada região resultante
desse processo, armazena as 8 orientações com suas magnitudes associadas.

Como mencionado anteriormente, essas etapas são realizadas com o propósito
de tornar eficiente a correlação entre os pontos chaves encontrados na imagem
com pontos-chave calculados a partir de um banco de imagens. Com isso, ao 
final dessas etapas, essa correlação é realizada para cada ponto-chave 
de forma independente e o melhor candidato para a correlação é definido 
pelo cálculo da distância Euclidiana mínima entre os vetores descritores.

De forma prática, esse método é bom para detectar pontos-chave e 
também para reconhecimento de objetos
em uma imagem, mesmo com variações de rotação, escala e até mesmo
oclusão. Essas características favorecem a utilização desse
método para atacar o problema do SLAM, como apresentam \citet{slamSIFT}
em sua pesquisa.

Após analisar os dois métodos de reconhecimento de pontos importantes
apresentados, podemos observar que pontos de baixo contrastes são
descartados. Essa limitação é comum para 
esse tipo de abordagem, baseada em pontos importantes, como é o caso do 
ARCore. No estudo feito por 
\citep{ARCoreCapabilities}, são realizados uma série de experimentos 
para testar as capacidades do ARCore e do ARKit,
a biblioteca de realidade aumentada da Apple. Em um desses testes,
foi analisada a quantidade de pontos importantes encontrados em diferentes
superfícies e constatado que numa 
parede de cor única, o ARCore não conseguiu encontrar
pontos suficientes para mapear a superfície vertical e o ARKit conseguiu 
mapear de forma parcial e demorou cerca de 40 segundos, dificultando
a utilização em tempo real. Sendo assim, fica inviável a utilização do ARCore 
para a identificação de planos verticais em paredes de cor única, um 
caso bem comum e fundamental para a funcionalidade de realidade aumentada
do \emph{Classic Blue}.

\section{Aprendizados}
\label{sec:ar_aprendizados}

Nas seções anteriores \ref{sec:slam} e \ref{sec:vislam}, vimos que o SLAM é,
em geral, um gargalo para a realidade aumentada devido a sua maior complexidade
computacional, porém a abordagem baseada em identificação de pontos importantes
é ruim para imagens com baixo contraste, que é o caso de uma parede de cor única,
fazendo com que essa etapa, chamada de \emph{frontend} seja o real gargalo.
Essa limitação faz com que o ARCore não seja indicado para o reconhecimento
de planos verticais com baixo contraste, levando os desenvolvedores e 
\emph{stakeholders} a buscarem alternativas como serão apresentadas no 
próximo capítulo.