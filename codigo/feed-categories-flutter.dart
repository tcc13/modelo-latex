class FeedCategories extends StatefulWidget {
  @override
  _FeedCategoriesState createState() => _FeedCategoriesState();
}

class _FeedCategoriesState extends State<FeedCategories> {
  final List<String> _categories = [
    'ALL',
    'TRENDING',
    'HOT',
    'SPONSORED',
  ];

  int _selectedCategory = 0;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 72,
      child: ListView.builder(
          padding: EdgeInsets.symmetric(horizontal: 4, vertical: 24),
          scrollDirection: Axis.horizontal,
          itemCount: _categories.length,
          itemBuilder: (context, index) {
            final String category = _categories[index];
            final bool isSelectedCategory = _selectedCategory == index;
            return FeedCategory(category, isSelectedCategory, () {
              setState(() {
                _selectedCategory = index;
              });
            });
          }),
    );
  }
}